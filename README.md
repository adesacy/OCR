# OCR

Quick script for ocerisation in R & Python using wrapper of Tesseract, the Google's optical character recognition engine.

## Features: 

The script takes a directory containing images and automatically transcribes them into raw text (txt). 
